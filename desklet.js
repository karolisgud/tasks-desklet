const Gio = imports.gi.Gio;
const St = imports.gi.St;
const Desklet = imports.ui.desklet;
const Lang = imports.lang;
const Mainloop = imports.mainloop;
const Clutter = imports.gi.Clutter;
const GLib = imports.gi.GLib;
const Tweener = imports.ui.tweener;
const Util = imports.misc.util;
const Main = imports.ui.main;

const Tooltips = imports.ui.tooltips;
const PopupMenu = imports.ui.popupMenu;
const Cinnamon = imports.gi.Cinnamon;
const Settings = imports.ui.settings;

const Soup = imports.gi.Soup;

function MyDesklet(metadata,decklet_id){
    this._init(metadata,decklet_id);
}

MyDesklet.prototype = {
    __proto__: Desklet.Desklet.prototype,


    _init: function(metadata,decklet_id){
        this.proces=null;

        try {
            Desklet.Desklet.prototype._init.call(this, metadata);
            this.settings = new Settings.DeskletSettings(this, "accudesk@logan", this.desklet_id);
            this.settings.bindProperty(Settings.BindingDirection.ONE_WAY,"zoom","zoom",this._refreshweathers,null);

            this.proces=true;
            this._refreshweathers();
        }
        catch (e) {
            global.logError(e);
        }
        return true;
    },

//##########################REFRESH#########################
    createwindow: function(){

        this.window=new St.BoxLayout({vertical: false});
        this.cweather = new St.BoxLayout({vertical: true});
        this.cwicon = new St.Bin({height: (500*this.zoom), width: (200*this.zoom)});
        this.cweather.add_actor(this.cwicon);
        this.window.add_actor(this.cweather);
    },

    _refreshweathers: function() {
        if (this.proces)
        {
            this.createwindow();
            this.setContent(this.window);
            this._timeoutId=Mainloop.timeout_add_seconds(600+ Math.round(Math.random()*120), Lang.bind(this, this._refreshweathers));
        }
    }
}

function main(metadata, decklet_id){
    let desklet = new MyDesklet(metadata,decklet_id);
    return desklet;
}
